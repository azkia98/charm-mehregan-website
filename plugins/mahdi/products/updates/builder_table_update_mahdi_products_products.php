<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMahdiProductsProducts extends Migration
{
    public function up()
    {
        Schema::table('mahdi_products_products', function($table)
        {
            $table->increments('id')->change();
        });
    }
    
    public function down()
    {
        Schema::table('mahdi_products_products', function($table)
        {
            $table->integer('id')->change();
        });
    }
}
