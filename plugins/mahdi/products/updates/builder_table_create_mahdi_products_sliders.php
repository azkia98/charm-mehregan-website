<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMahdiProductsSliders extends Migration
{
    public function up()
    {
        Schema::create('mahdi_products_sliders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mahdi_products_sliders');
    }
}
