<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMahdiProductsSlides extends Migration
{
    public function up()
    {
        Schema::rename('mahdi_products_sliders', 'mahdi_products_slides');
        Schema::table('mahdi_products_slides', function($table)
        {
        });
    }
    
    public function down()
    {
        Schema::rename('mahdi_products_slides', 'mahdi_products_sliders');
        Schema::table('mahdi_products_sliders', function($table)
        {
        });
    }
}
