<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMahdiProductsAtributies2 extends Migration
{
    public function up()
    {
        Schema::table('mahdi_products_atributies', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('title')->change();
        });
    }
    
    public function down()
    {
        Schema::table('mahdi_products_atributies', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->string('title', 191)->change();
        });
    }
}
