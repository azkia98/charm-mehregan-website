<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMahdiProductsProducts5 extends Migration
{
    public function up()
    {
        Schema::table('mahdi_products_products', function($table)
        {
            $table->string('material')->nullable();
            $table->dropColumn('type');
        });
    }
    
    public function down()
    {
        Schema::table('mahdi_products_products', function($table)
        {
            $table->dropColumn('material');
            $table->string('type', 191)->nullable()->default('NULL');
        });
    }
}
