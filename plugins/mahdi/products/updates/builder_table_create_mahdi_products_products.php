<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMahdiProductsProducts extends Migration
{
    public function up()
    {
        Schema::create('mahdi_products_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id')->unsigned();
            $table->text('title');
            $table->text('description');
            $table->text('code');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mahdi_products_products');
    }
}
