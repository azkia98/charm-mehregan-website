<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMahdiProductsProducts2 extends Migration
{
    public function up()
    {
        Schema::table('mahdi_products_products', function($table)
        {
            $table->string('type')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('mahdi_products_products', function($table)
        {
            $table->dropColumn('type');
        });
    }
}
