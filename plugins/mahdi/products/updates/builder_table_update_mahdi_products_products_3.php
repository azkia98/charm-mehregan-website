<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMahdiProductsProducts3 extends Migration
{
    public function up()
    {
        Schema::table('mahdi_products_products', function($table)
        {
            $table->smallInteger('sex')->unsigned()->default(0);
            $table->string('type')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('mahdi_products_products', function($table)
        {
            $table->dropColumn('sex');
            $table->string('type', 191)->default('NULL')->change();
        });
    }
}
