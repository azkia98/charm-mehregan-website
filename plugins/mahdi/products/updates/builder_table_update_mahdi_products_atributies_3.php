<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMahdiProductsAtributies3 extends Migration
{
    public function up()
    {
        Schema::table('mahdi_products_atributies', function($table)
        {
            $table->dropColumn('slide_id');
        });
    }
    
    public function down()
    {
        Schema::table('mahdi_products_atributies', function($table)
        {
            $table->integer('slide_id');
        });
    }
}
