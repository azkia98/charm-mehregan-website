<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMahdiProductsProducts4 extends Migration
{
    public function up()
    {
        Schema::table('mahdi_products_products', function($table)
        {
            $table->string('type')->default(null)->change();
            $table->renameColumn('sex', 'gender');
        });
    }
    
    public function down()
    {
        Schema::table('mahdi_products_products', function($table)
        {
            $table->string('type', 191)->default('NULL')->change();
            $table->renameColumn('gender', 'sex');
        });
    }
}
