<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMahdiProductsAtributiesSlides extends Migration
{
    public function up()
    {
        Schema::create('mahdi_products_atributies_slides', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('slide_id')->unsigned();
            $table->integer('atribute_id')->unsigned();
            $table->primary(['slide_id','atribute_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mahdi_products_atributies_slides');
    }
}
