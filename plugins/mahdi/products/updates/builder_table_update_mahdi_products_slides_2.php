<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMahdiProductsSlides2 extends Migration
{
    public function up()
    {
        Schema::table('mahdi_products_slides', function($table)
        {
            $table->string('title')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('mahdi_products_slides', function($table)
        {
            $table->dropColumn('title');
        });
    }
}
