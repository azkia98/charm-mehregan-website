<?php namespace Mahdi\Products\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMahdiProductsAtributies extends Migration
{
    public function up()
    {
        Schema::table('mahdi_products_atributies', function($table)
        {
            $table->integer('slide_id');
            $table->string('title')->change();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
    
    public function down()
    {
        Schema::table('mahdi_products_atributies', function($table)
        {
            $table->dropColumn('slide_id');
            $table->string('title', 191)->change();
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
        });
    }
}
