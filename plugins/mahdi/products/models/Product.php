<?php namespace Mahdi\Products\Models;

use Model;
use October\Rain\Database\Traits\Sortable;

/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use Sortable;

    const SORT_ORDER = 'title';
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mahdi_products_products';

    public $attachOne = [
        'image' => 'System\Models\File'
    ];

}
