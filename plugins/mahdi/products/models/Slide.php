<?php namespace Mahdi\Products\Models;

use Model;
use System\Models\File;

/**
 * Model
 */
class Slide extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    // public $timestamps = false;
    
    const SORT_ORDER = 'id';
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    protected $guarded = [];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mahdi_products_slides';


    public $belongsToMany = [
        'atributies' => [
            'Mahdi\Products\Models\Atributie',
            'table'     => 'mahdi_products_atributies_slides',
            'key'       => 'slide_id',
            'otherKey'  => 'atribute_id'
        ]
    ];


    public $attachOne = [
        'img' => File::class
    ];
}
