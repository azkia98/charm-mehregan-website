<?php namespace Mahdi\Products\Models;

use Model;

/**
 * Model
 */
class Atributie extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mahdi_products_atributies';

    public $belongsToMany = [
        'slides' => [
            'Mahdi\Products\Models\Slide',
            'table'    => 'mahdi_products_atributies_slides',
            'key'      => 'atribute_id',
            'otherKey' => 'slide_id'
        ]
    ];


    public function getTypesOptions()
    {
        return Slide::all();
    }
}
